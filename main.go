package main

import (
	"github.com/caarlos0/env/v6"
	"gitlab.com/soerenschneider/sprint-name-generator/lib"
	"log"
)

func main() {
	config := lib.Config{
	}

	if err := env.Parse(&config); err != nil {
		log.Fatalf("%+v\n", err)
	}

	config.LogConfig()
	if err := config.Validate(); err != nil {
		log.Fatalf("config invalid: %s", err.Error())
	}

	themes, err := lib.ParseContent(config.ContentPath); if err != nil {
		log.Fatalf("could not open content file: %s", err.Error())
	}
	
	router := lib.SprintGenerator{Config: &config, Themes: themes.AsMap()}
	router.SprintGenerator()
}