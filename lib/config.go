package lib

import (
	"log"
	"os"
)

type Config struct {
	ErrorTemplatePath      string `env:"TEMPL_ERROR" envDefault:"resources/templates/error.html"`
	OverviewTemplatePath   string `env:"TEMPL_OVERVIEW" envDefault:"resources/templates/overview.html"`
	SuggestionTemplatePath string `env:"TEMPL_SUGGESTION" envDefault:"resources/templates/template.html"`
	Listen                 string `env:"LISTEN" envDefault:":8080"`
	ContentPath            string `env:"CONTENT,required"`
}

func (c *Config) Validate() error {
	files := []string {
		c.ErrorTemplatePath,
		c.OverviewTemplatePath,
		c.SuggestionTemplatePath,
		c.ContentPath,
	}

	for _, file := range files {
		if _, err := os.Stat(file); os.IsNotExist(err) {
			return err
		}
	}

	return nil
}

func (c *Config) LogConfig() {
	log.Println("Using configuration")
	log.Printf("ContentPath=%s", c.ContentPath)
	log.Printf("ErrorTemplatePath=%s", c.ErrorTemplatePath)
	log.Printf("OverviewTemplatePath=%s", c.OverviewTemplatePath)
	log.Printf("SuggestionTemplatePath=%s", c.SuggestionTemplatePath)
	log.Printf("Listen=%s", c.Listen)
}
