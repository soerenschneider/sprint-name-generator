package lib

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"regexp"
	"strings"
)

const themePathVariable = "theme"
var reg = regexp.MustCompile("[^a-zA-Z0-9]+")

type Themes struct {
	Themes []Theme `json:"Themes"`
}

type Theme struct {
	Name    string   `json:"Name"`
	Choices []string `json:"Choices"`
}

func (t* Themes) AsMap() map[string]*Theme {
	ret := make(map[string]*Theme)
	for _, theme := range t.Themes {
		themeName := GetKey(theme.Name)
		ret[themeName] = &theme
	}
	return ret
}

func GetKey(key string) string {
	key = strings.ToLower(key)
	return reg.ReplaceAllString(key, "")
}

func (t *Theme) GetRandomChoice() (string, error) {
	cnt := len(t.Choices)
	if cnt == 0 {
		return "", errors.New("Empty Choices")
	}
	randomIndex := rand.Intn(cnt)
	return t.Choices[randomIndex], nil
}

func ParseContent(file string) (*Themes, error) {
	log.Printf("Trying to parse Themes from %s", file)
	jsonFile, err := os.Open(file); if err != nil {
		return nil, err
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	log.Println("Successfully read file")

	var themes Themes
	err = json.Unmarshal(byteValue, &themes)
	log.Printf("Loaded %d Themes", len(themes.Themes))

	return &themes, err
}