OUTPUT_DIR ?= build
BIN ?= sprint-name-generator
COVER_OUTPUT ?= cover.out

build: output-dir
	go build -o ${OUTPUT_DIR}/${BIN} .

test: clean-test
	go test ./... -coverprofile ${COVER_OUTPUT}

clean: clean-build clean-test

clean-build:
	rm -rf ${OUTPUT_DIR}

clean-test:
	rm -rf ${COVER_OUTPUT}

output-dir:
	mkdir -p ${OUTPUT_DIR}

.PHONY: build test