FROM docker.io/golang:1.13 AS builder
COPY . /source/
WORKDIR /source/
RUN CGO_ENABLED=0 go build

FROM alpine
WORKDIR /spg
COPY resources /spg/resources/
COPY --from=builder /source/sprint-name-generator /spg/sprint-name-generator
ENTRYPOINT /spg/sprint-name-generator
