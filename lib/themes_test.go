package lib

import (
	"reflect"
	"testing"
)

func TestTheme_getRandomTheme(t1 *testing.T) {
	type fields struct {
		name    string
		choices []string
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		wantErr bool
	}{
		{
			fields: fields{
				name:    "Example",
				choices: []string{},
			},
			want: "",
			wantErr: true,
		},
		{
			fields: fields{
				name:    "Example",
				choices: []string{"A"},
			},
			want:    "A",
			wantErr: false,
		},
		{
			fields: fields{
				name:    "Example",
				choices: []string{"A", "B"},
			},
			want:    "",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Theme{
				Name:    tt.fields.name,
				Choices: tt.fields.choices,
			}
			got, err := t.GetRandomChoice()
			if (err != nil) != tt.wantErr {
				t1.Errorf("getRandomChoice() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(tt.want) > 0 && got != tt.want {
				t1.Errorf("getRandomChoice() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParse(t *testing.T) {
	tests := []struct {
		name    string
		want    *Themes
		path   string
		wantErr bool
	}{
		{
			want: &Themes{
				Themes: []Theme{
					{
						Name:    "Example",
						Choices: []string{"Bla", "Blub"},
					},
				},
			},
			path: "../resources/test_input.json",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseContent(tt.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseContent() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseContent() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getKey(t *testing.T) {
	tests := []struct {
		name string
		key string
		want string
	}{
		{
			key: "What a stupid key",
			want: "whatastupidkey",
		},
		{
			key: "",
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetKey(tt.key); got != tt.want {
				t.Errorf("getKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestThemes_asMap(t1 *testing.T) {
	type fields struct {
		Themes []Theme
	}
	tests := []struct {
		name   string
		themes []Theme
		want   map[string]*Theme
	}{
		{
			themes: []Theme{
				{Name: "Example", Choices: []string{"Bla", "Blub"}},
			},
			want: map[string]*Theme{"example": &Theme{Name: "Example", Choices:[]string{"Bla","Blub"}}},
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Themes{
				Themes: tt.themes,
			}
			if got := t.AsMap(); !reflect.DeepEqual(got, tt.want) {
				t1.Errorf("AsMap() = %v, want %v", got, tt.want)
			}
		})
	}
}