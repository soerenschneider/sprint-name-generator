package lib

import (
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"time"
)

type Render struct {
	Theme string
	Title string
}

type SprintGenerator struct {
	Config *Config
	Themes map[string]*Theme
}

func (router *SprintGenerator) SprintGenerator() {
	r := mux.NewRouter()
	r.HandleFunc("/", router.handlerOverview)
	r.HandleFunc("/gen/{theme}", router.handlerTheme)
	log.Println("Starting..")
	srv := &http.Server{
		Handler:      r,
		Addr:         router.Config.Listen,
		WriteTimeout: 2 * time.Second,
		ReadTimeout:  5 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}

func (s *SprintGenerator) handlerOverview(w http.ResponseWriter, r *http.Request) {
	log.Printf("Connection from %s for %s", r.RemoteAddr, r.RequestURI)
	tmpl := template.Must(template.ParseFiles(s.Config.OverviewTemplatePath))
	tmpl.Execute(w, s.Themes)
}

func (s *SprintGenerator) handlerTheme(w http.ResponseWriter, r *http.Request) {
	log.Printf("Connection from %s for %s", r.RemoteAddr, r.RequestURI)
	vars := mux.Vars(r)

	desiredTheme := vars[themePathVariable]
	key := GetKey(desiredTheme)
	theme, found := s.Themes[key]
	var tmpl *template.Template
	var choice string
	if !found {
		tmpl = template.Must(template.ParseFiles(s.Config.ErrorTemplatePath))
	} else {
		choice, _ = theme.GetRandomChoice()
		tmpl = template.Must(template.ParseFiles(s.Config.SuggestionTemplatePath))
	}

	data := Render{Title: choice, Theme: desiredTheme}
	tmpl.Execute(w, data)
}
